const request = require('request-promise-native')
const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function execute(cmd) {
    console.log('executing:', cmd)
    const {stdout, stderr} = await exec(cmd);

    if (stdout) console.log('stdout:', stdout);
    if (stderr) console.log('stderr:', stderr);
}

describe('view cache service',  () => {

    test('should load resources from view-cache ', async () => {
        await execute('kubectl apply -f ./back-end-gateway-with-no-fault-injected.yaml')
        const result = JSON.parse(await request({timeout:1000, uri: 'http://35.238.78.41/get-data?key=11'}))
        expect(result).toBeInstanceOf(Array)
        expect(result.length).toBe(4)
    })

    //
    test('should load empty object if back end service is unreachable', async (done) => {
        await execute('echo $PATH')

        await execute('kubectl apply -f ./back-end-gateway-with-fault-injected.yaml')
        const promise = request({timeout:1000, uri:'http://35.238.78.41/get-data?key=11'})
        promise.then(function(value) {

            expect(false).toBeTruthy()
            done()
        }).catch(function(value) {
            expect(true).toBeTruthy()
            done()
            // expected output: "foo"
        })

    })
})

afterEach(async () => {
    await execute('kubectl apply -f ../../back-end/back-end-gateway.yaml')
});
